<?php
    session_start();
    if (isset($_SESSION['id'])){
    header('Location: dashboard.php');
    }
    require_once './vendor/autoload.php';
    use App\Classes\Login;
    $login= new Login();
    if(isset($_POST['btn'])){
    $login->adminLoginCheck();
    };
?>

<style>
    .form-container{
        height: 400px;
        width: 40%;
        margin: 100px auto;
        box-shadow: 0 0 5px 5px grey;
        box-sizing: border-box;
        padding: 100px;
    }
</style>
<div class="form-container">
    <form action="" method="POST">
        <table>
            <tr>
                <th>Email Address</th>
                <td>
                    <input type="email" name="email">
                </td>
            </tr>
            <tr>
                <th>Password</th>
                <td>
                    <input type="password" name="password">
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <input type="submit" name="btn" value="Submit">
                </td>
            </tr>
        </table>
    </form>
</div>